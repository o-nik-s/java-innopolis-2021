package homework;

import java.util.Random;

public class Controller {
//    телевизор
    private TV tv;

    public Controller(TV tv) {
        this.tv = tv;
    }

    public void on(int number) {
        Random random = new Random(System.currentTimeMillis());
        int numberProgram = random.nextInt(tv.channels[number].programCounts) + 1;
        System.out.println("На канале " + tv.channels[number].channel + " идет передача " + tv.channels[number].programs[numberProgram].name);
    }
}
