package homework;


public class Channel {
//    название
//    список передач
    public String channel;
    public Program[] programs;

    public int programCounts;

    public Channel(String channel) {
        this.channel = channel;
        this.programs = new Program[24];
        this.programCounts = 0;
    }

    public void addProgram(Program program) {
        programCounts++;
        this.programs[programCounts] = program;
    }
}

