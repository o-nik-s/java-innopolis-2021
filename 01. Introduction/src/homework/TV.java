package homework;

public class TV {
//    модель
//    список каналов
    public String model;
    public Channel[] channels;

    public int channelsCount;

    public TV(String model) {
        this.model = model;
        this.channels = new Channel[16];
        this.channelsCount = 0;
    }

    public void addChannel(Channel channel) {
        channelsCount++;
        this.channels[channelsCount] = channel;
    }
}
