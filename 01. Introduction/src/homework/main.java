package homework;

import java.util.Scanner;

public class main {
    public static void main(String[] args){

        TV tv = getTv();

        Controller controller = new Controller(tv);

        int channelNumber = channelChoice(tv, controller);
        if (channelNumber!=-99999) {
            controller.on(channelNumber);
        }

    }

    private static int channelChoice(TV tv, Controller controller) {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Привет. В телевизоре %s работают каналы: \n", tv.model);
        for (int i = 1; i <= tv.channelsCount; i++){
            System.out.printf("%d - %s \n", i, tv.channels[i].channel);
        }
        System.out.println("Выберите номер канала: ");
        try {
            int channelNumber = scanner.nextInt();
            if ((1 <= channelNumber) & (channelNumber <= tv.channelsCount)) {
                return channelNumber;
            } else {
                System.out.println("Такого канала нет в телевизоре.");
            }
        }
        catch(Exception e)
        {
            System.out.println("Вы ошиблись!");
        }
        return -99999;
    }

    private static TV getTv() {
        TV tv = new TV("Samsung");

        Channel channel1 = new Channel("Первый");
        Program p11 = new Program("Давай поженимся");
        Program p12 = new Program("Малахов+");
        Program p13 = new Program("Пусть говорят");

        Channel channel2 = new Channel("Второй");
        Program p21 = new Program("Сто к одному");
        Program p22 = new Program("Устами младенца");
        Program p23 = new Program("Вести");
        Program p24 = new Program("Форд Боярд");
        Program p25 = new Program("Спокойной ночи, малыши!");

        channel1.addProgram(p11);
        channel1.addProgram(p12);
        channel1.addProgram(p13);
        tv.addChannel(channel1);

        channel2.addProgram(p21);
        channel2.addProgram(p22);
        channel2.addProgram(p23);
        channel2.addProgram(p24);
        channel2.addProgram(p25);
        tv.addChannel(channel2);

        return tv;
    }
}
